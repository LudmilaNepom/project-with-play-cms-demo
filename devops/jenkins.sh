#!/usr/bin/env bash

source $(dirname $0)/common.sh

if [[ -n "$@" ]]; then
    CMD="$@"
else
    CMD="up -d"
fi

DOCKER_BIN=$DOCKER_BIN \
HOST_WORKDIR=$HOST_WORKDIR \
   docker-compose -p $COMPOSE_PROJECT_NAME -f jenkins.yml $CMD
