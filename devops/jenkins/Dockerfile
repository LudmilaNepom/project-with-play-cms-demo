FROM jenkins:1.642.4

ENV JAVA_OPTS -Dhudson.footerURL=http://www.insign.ch

USER root

RUN apt-get update

# Install Docker
RUN curl -sSL https://get.docker.com/gpg | apt-key add -
RUN curl -sSL https://get.docker.com/ | sh

# Install Docker Compose
ENV DOCKER_COMPOSE_VERSION 1.6.2
RUN curl -L https://github.com/docker/compose/releases/download/${DOCKER_COMPOSE_VERSION}/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
RUN chmod +x /usr/local/bin/docker-compose

# Install SBT
ENV SBT_VERSION 0.13.8
RUN wget https://dl.bintray.com/sbt/debian/sbt-${SBT_VERSION}.deb \
    && dpkg -i sbt-${SBT_VERSION}.deb \
    && rm sbt-${SBT_VERSION}.deb

# Copy util script to extact jenkins plugin name:id from pom.properies
COPY mvn-artifact.sh /usr/local/bin/mvn-artifact.sh
RUN chmod +x /usr/local/bin/mvn-artifact.sh

# Replace default entrypoint script
COPY jenkins.sh /usr/local/bin/jenkins.sh
RUN chmod +x /usr/local/bin/jenkins.sh

# Replace default plugin installation script
COPY plugins.sh /usr/local/bin/plugins.sh
RUN chmod +x /usr/local/bin/plugins.sh

RUN mkdir /var/releases
RUN chown -R jenkins:jenkins /var/releases
VOLUME /var/releases

USER jenkins

COPY executors.groovy /usr/share/jenkins/ref/init.groovy.d/executors.groovy
COPY jobs.groovy /usr/share/jenkins/ref/init.groovy.d/jobs.groovy

COPY build_config.xml /var/jenkins_home/templates/build_config.xml

COPY plugins.txt /usr/share/jenkins/ref/
RUN /usr/local/bin/plugins.sh /usr/share/jenkins/ref/plugins.txt

USER root