#!/bin/bash

# This value is assigned to an active user in docker container to ensure consistent
# file permissions on mounted host volumes.
# This value is only useful for Linux hosts when active user in docker container is not root.
WORKDIR_UID=""
if [ "$(uname)" != "Darwin" ] && [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
   WORKDIR_UID=`stat -c "%u" .`
fi
export WORKDIR_UID


# This value is prepended along with the service name to the container container on start up.
# Also app’s network is given the same name as the project name.
# See https://docs.docker.com/compose/reference/overview/#compose-project-name
COMPOSE_PROJECT_NAME=playcmsdemo
export COMPOSE_PROJECT_NAME


DOCKER_BIN=`which docker`
export DOCKER_BIN

# Store the work directory of a host machine to properly map the folder from jenkins container to app container
if [ -z "$HOST_WORKDIR" ]; then
    HOST_WORKDIR=`dirname $(pwd)`
    export HOST_WORKDIR
fi
