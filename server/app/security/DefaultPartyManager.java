package security;

import ch.insign.playauth.party.Party;
import models.User;

public class DefaultPartyManager extends ch.insign.playauth.party.support.DefaultPartyManager {
    @Override
    public Class<? extends Party> getPartyClass() {
        return User.class;
    }
}
