package crud.models;

import ch.insign.commons.db.Model;
import com.uaihebert.model.EasyCriteria;
import play.data.format.Formats;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@NamedQuery(name = "Car.getLastSales", query = "SELECT c FROM Car c WHERE c.buyDate is not null order by c.buyDate")
@Table(name = "example_crud_car")
public class Car extends Model {
    public static CarFinder find = new CarFinder();

    @Constraints.Required
    @Constraints.MaxLength(value = 48)
    private String model;

    @Formats.DateTime(pattern = "yyyy-MM-dd HH:mm")
    @Temporal(TemporalType.TIMESTAMP)
    private Date buyDate;

    @Constraints.Required
    @Constraints.MaxLength(value = 10)
    private String registrationId;

    @Constraints.Min(value = 0, message = "example.crud.car.price.error.message")
    private float price;

    @ManyToOne
    private Brand brand;

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public String getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }

    public Date getBuyDate() {
        return buyDate;
    }

    public void setBuyDate(Date buyDate) {
        this.buyDate = buyDate;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    /**
     * CarFinder class. Contains methods to find cars from the database.
     */
    public static class CarFinder extends Model.Finder<Car> {

        CarFinder() {
            super(Car.class);
        }

        public EasyCriteria<Car> findByBrand(String brandId) {
            Brand brand = Brand.find.byId(brandId);

            return query().andEquals("brand", brand);
        }

        public List<Car> findLastSales (int count){
            TypedQuery<Car> namedQuery = jpaApi.em().createNamedQuery("Car.getLastSales", Car.class);
            return namedQuery.setMaxResults(count).getResultList();
        }
    }
}
