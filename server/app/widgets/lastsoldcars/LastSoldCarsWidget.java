package widgets.lastsoldcars;

import ch.insign.cms.models.FrontendWidget;
import ch.insign.commons.filter.Filterable;
import ch.insign.playauth.PlayAuth;
import ch.insign.playauth.party.Party;
import crud.models.Car;
import play.db.jpa.Transactional;
import play.twirl.api.Html;

import java.util.List;

public class LastSoldCarsWidget extends FrontendWidget {
    private static final String TAG = "lastSoldCarsWidget";
    private static final int MAX_CARS = 5;

    @Override
    public String[] filterTags() {
        return new String[] {TAG};
    }

    @Override
    @Transactional(readOnly = true)
    public Html render(String tag, List<String> params, Filterable source) {
        List<Car> cars = Car.find.findLastSales(MAX_CARS);
        int carsMaxSize = MAX_CARS;

        if (params.size() > 1) {
            try {
                carsMaxSize = Integer.parseInt(params.get(1));
            } catch (NumberFormatException e) {}
        }
        if (carsMaxSize < cars.size()) {
            cars = cars.subList(0, carsMaxSize);
        }

        return widgets.lastsoldcars.html.last_sold_cars.render(cars);
    }
}


