package configuration;

import ch.insign.cms.models.CMS;
import ch.insign.commons.db.MString;
import ch.insign.cms.models.MStringDataBinder;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import models.MyAuthSetup;
import models.MyCmsSetup;
import play.data.format.Formatters;

import static ch.insign.commons.db.DataBinding.registerDataBinder;

@Singleton
public class OnStartLoader {

    @Inject
    private OnStartLoader(Formatters formatters) {
        registerDataBinder(MString.class, new MStringDataBinder());
        // Set default AuthSetup class which initialize default roles and parties on first run.
        CMS.setAuthSetup(new MyAuthSetup());
        // CMS app start setup:
        // Set our own config/setup instances
        CMS.setSetup(new MyCmsSetup());

        CMS.getSetup().onAppStart(formatters);
    }
}
