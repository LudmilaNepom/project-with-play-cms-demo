package configuration;

import com.google.inject.AbstractModule;

public class Module extends AbstractModule {
    @Override
    protected void configure() {
        bind(OnStartLoader.class).asEagerSingleton();
    }
}
