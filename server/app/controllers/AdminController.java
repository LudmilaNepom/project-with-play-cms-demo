package controllers;

import ch.insign.cms.controllers.GlobalActionWrapper;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;


@With({GlobalActionWrapper.class})
@Transactional
public class AdminController extends Controller {

    public Result myApp() {
        return ok(views.html.admin.myapp.main.render());
    }

}
