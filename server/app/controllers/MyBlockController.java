package controllers;

import blocks.myblock.MyContentBlock;
import ch.insign.cms.models.AbstractBlock;
import ch.insign.cms.utils.Error;
import ch.insign.commons.db.MString;
import ch.insign.playauth.PlayAuth;
import com.google.inject.Inject;
import play.data.DynamicForm;
import play.data.FormFactory;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;


public class MyBlockController extends Controller {
    private final FormFactory formFactory;
    @Inject
    public MyBlockController(FormFactory formFactory) {
        this.formFactory = formFactory;
    }
    @Transactional
    public Result doEditMyContentBlock(String id) {
        AbstractBlock targetBlock;
            targetBlock = AbstractBlock.find.byId(id);
            if (targetBlock == null) {
                return internalServerError("Block not found: " + id);
            }
            if (!targetBlock.canModify()) {
                return Error.forbidden("Permission error: User '" + PlayAuth.getCurrentParty() +
                        "' tried to save block " + targetBlock + " without permission.");
            }
        DynamicForm requestData = formFactory.form().bindFromRequest();
        ((MyContentBlock)targetBlock).getCars().set(ctx().lang().language(), requestData.get("cars"));
        return ok(requestData.get("cars"));
}
}
