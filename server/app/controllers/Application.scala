package controllers

import java.nio.ByteBuffer
import javax.inject._
import play.db.jpa.Transactional
import play.db.jpa.JPA
import play.db.jpa.JPAApi

import boopickle.Default._

import play.api.mvc._
import playcms.shared.Api
import service.ApiService


import scala.concurrent.ExecutionContext.Implicits.global

object Router extends autowire.Server[ByteBuffer, Pickler, Pickler] {
  override def read[R: Pickler](p: ByteBuffer) = Unpickle[R].fromBytes(p)
  override def write[R: Pickler](r: R) = Pickle.intoBytes(r)
}

@Singleton
@Transactional
class Application @Inject() (jpa: JPAApi, apiService: ApiService) extends Controller {

  /**
    * Autowire API interface.
    * Here the un-pickling of scala-js objects take place and the corresponding api-method is called.
    * Actual implementation of the API can be found under service.ApiService
    * @param path
    * @return
    */
  def autowireApi(path: String) = Action.async(parse.raw) {
    implicit request =>
      //println(s"Request path: $path")

      // get the request body as ByteString
      val b = request.body.asBytes(parse.UNLIMITED).get

      // call Autowire route
      Router.route[Api](apiService)(
        autowire.Core.Request(path.split("/"), Unpickle[Map[String, ByteBuffer]].fromBytes(b.asByteBuffer))
      ).map(buffer => {
        val data = Array.ofDim[Byte](buffer.remaining())
        buffer.get(data)
        Ok(data)
      })
  }

  /**
    * Receives logging events from javascript (client side).
    * Remove this if you don't want to have logs from browser-clients
    * TODO: replace println method with a proper log-statement
    * @return
    */
  def logging = Action(parse.anyContent) {
    implicit request =>
      request.body.asJson.foreach { msg =>
        //println(s"CLIENT - $msg")
      }
      Ok("")
  }
}
