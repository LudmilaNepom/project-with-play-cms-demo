package service

import java.util.function.Supplier
import javax.inject._
import play.db.jpa.{JPA, JPAApi}
import ch.insign.playauth.PlayAuth
import play.mvc.With
import playcms.shared.{Api, UserItem}
import scala.collection.JavaConversions._

@With(Array(classOf[Nothing]))
class ApiService @Inject() (jPAApi: JPAApi) extends Api {

  // message of the day
  override def simpleAjaxCall(name: String): String = "this is a stupid string originating from server"


  override def getUsers(): Seq[UserItem] = {

    // FIXME: Next method works but is deprecated.
    // Should be replaced with jPAApi.withTransaction(new Supplier[Seq[UserItem]]{ ... });
    // but before that we need to change using EntityManager from "JPA.em()" to "jpaApi.em()"
    // in all places, as it is required in documentation, otherwise we'll have "No Entity Manager exception"
    // @see {https://www.playframework.com/documentation/2.5.x/JavaJPA#Using-play.db.jpa.JPAApi}
    //
    // The other workaround for this is to add next binding in CustomApplicationLoader (may be slow and unstable):
    // bind(JPAApi.class).to((Provider<JPAApi>) () -> JPA.createFor("defaultPersistenceUnit")),

    JPA.jpaApi().withTransaction(new Supplier[Seq[UserItem]]{
      override def get(): Seq[UserItem] = {
        val users = PlayAuth.getPartyManager.findAll
        users.map(u => UserItem(u.getId, u.getName, u.getEmail)).toSeq
      }
    })
  }
}
