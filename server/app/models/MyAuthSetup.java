package models;

import ch.insign.cms.models.AuthSetup;
import ch.insign.cms.permissions.ApplicationPermission;
import ch.insign.cms.permissions.BlockPermission;
import ch.insign.playauth.PlayAuth;
import ch.insign.playauth.authz.AccessControlManager;
import ch.insign.playauth.authz.SecurityIdentity;
import ch.insign.playauth.party.*;
import ch.insign.playauth.party.support.DefaultPartyRole;

public class MyAuthSetup extends AuthSetup {

    // Predefined Demo Role
    public static final String ROLE_DEMO_ROLE = "DemoRole";

    @Override
    protected void createRoles() {
        super.createRoles();

        // Create DemoRole
        SecurityIdentity demoRoleSid = PlayAuth.getSecurityIdentity(
                PlayAuth.getPartyRoleManager().create(ROLE_DEMO_ROLE));

        // Add default permissions to new DemoRole
        AccessControlManager acm = PlayAuth.getAccessControlManager();
        acm.allowPermission(demoRoleSid, ApplicationPermission.BROWSE_BACKEND);
        acm.allowPermission(demoRoleSid, BlockPermission.MODIFY);
    }

    @Override
    protected void createParties() {
        super.createParties();
        createParty("admin", "admin@insign.ch", "123456", ISOGender.MALE, DefaultPartyRole.ROLE_SUPERUSER);
        createParty("demouser", "demouser@insign.ch", "123456", ISOGender.MALE, DefaultPartyRole.ROLE_USER);
    }
}
