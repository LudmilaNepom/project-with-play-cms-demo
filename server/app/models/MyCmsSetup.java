package models;

import blocks.errorblock.DefaultErrorPage;
import blocks.myblock.MyContentBlock;
import blocks.mypageblock.MyPageBlock;
import blocks.pageblock.DefaultPageBlock;
import blocks.teaserblock.TeaserBlock;
import ch.insign.cms.blocks.backendlinkblock.BackendLinkBlock;
import ch.insign.cms.blocks.errorblock.ErrorPage;
import ch.insign.cms.blocks.groupingblock.GroupingBlock;
import ch.insign.cms.blocks.linkblock.LinkBlock;
import ch.insign.cms.blocks.searchresultblock.SearchResultBlock;
import ch.insign.cms.blocks.sliderblock.SliderCollectionBlock;
import ch.insign.cms.blocks.horizontalcollection.HorizontalCollectionBlock;
import ch.insign.cms.models.*;
import ch.insign.cms.service.EmailService;
import ch.insign.commons.db.MString;
import ch.insign.commons.db.Paginate;
import ch.insign.commons.i18n.Language;
import crud.block.CarInventoryBlock;
import crud.models.Brand;
import crud.models.Car;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.twirl.api.Html;
import views.html.email_layout;
import widgets.lastsoldcars.LastSoldCarsWidget;
import widgets.registeredusers.RegisteredUsersWidget;

import java.time.Instant;
import java.util.Date;

public class MyCmsSetup  extends Setup {
    private static final String ERROR_500 = "500 Application error";
    private static final String ERROR_EXPIRED = "404 page not found";
    private static final String ERROR_404 = "404 page not found";
    private static final String ERROR_FORBIDDEN = "403 page forbidden";
    private static final Logger logger = LoggerFactory.getLogger(MyCmsSetup.class);

    @Override
    public void registerPartyFormManager() {

        // register party form manager to specify user's custom edit and create forms
        CMS.setPartyFormManager(new MyUserHandler());
    }

    @Override
    public void registerPartyEventHandler() {

        // register party event handler handles party's CRUD events
        CMS.setPartyEvents(new MyUserHandler());
    }

    @Override
    public void addAdminBackendNavigation() {

        super.addAdminBackendNavigation();

        PageBlock root = PageBlock.find.byKey(PageBlock.KEY_BACKEND);

        // Adding our example apps to the backend

        String lang = Language.getCurrentLanguage();
        String key = "_backend_myapp";

        if (BackendLinkBlock.find.byKey(key) == null) {
            BackendLinkBlock page = new BackendLinkBlock();
            root.getSubBlocks().add(page);
            page.setParentBlock(root);
            page.setKey(key);
            page.getLinkTarget().set(lang, "/admin/myapp");
            page.getNavTitle().set("en", "My app");
            page.getNavTitle().set("de", "Meine app");
            page.setLinkIcon("link");
            page.save();
        }

        String crud_page_key = "_backend_crud_example";

        if (BackendLinkBlock.find.byKey(crud_page_key) == null) {
            BackendLinkBlock page = new BackendLinkBlock();
            root.getSubBlocks().add(page);
            page.setParentBlock(root);
            page.setKey(crud_page_key);
            page.getLinkTarget().set(lang, crud.controllers.routes.CarController.list(1, Paginate.DEFAULT_ITEMS_PER_PAGE, "").url());
            page.getNavTitle().set("en", "CRUD Example");
            page.getNavTitle().set("de", "CRUD Example");
            page.setLinkIcon("link");
            page.save();
        }
    }

    @Override
    public void registerBlocks() {
        CMS.getBlockManager().register(
                // base cms blocks
                ContentBlock.class,
                CollectionBlock.class,
                LinkBlock.class,
                BackendLinkBlock.class,
                GroupingBlock.class,
                SliderCollectionBlock.class,
                HorizontalCollectionBlock.class,
                SearchResultBlock.class,
                ErrorPage.class,
                // project specific blocks
                DefaultPageBlock.class,
                TeaserBlock.class,
                CarInventoryBlock.class,
                MyContentBlock.class,
                MyPageBlock.class
        );
    }

    @Override
    public void registerContentFilters() {
        super.registerContentFilters();
        CMS.getFilterManager().register(
                new RegisteredUsersWidget()
        );
        CMS.getFilterManager().register(
                new LastSoldCarsWidget()
        );
    }

    @Override
    public void registerEmailService() {
        // Override render method of default EmailService to set custom layout for mails
        CMS.registerEmailService(new EmailService() {
            @Override
            protected Html render(String content, String language) {
                return email_layout.render(content);
            }
        });
    }

    @Override
    public void addExampleData() {
        super.addExampleData();
        createTeaserBlocks();
        createWidgetExampleBlock();
        createErrorTemplates();
        createCrudExampleData();
        createCarInventoryBlock();
    }

    @Override
    public ErrorPage getErrorPageInstance() {
        return new DefaultErrorPage();
    }

    @Override
    public ErrorPage createErrorPageNotFound() {
        ErrorPage errorPage = super.createErrorPageNotFound();
        createErrorPageContent(errorPage, ERROR_404);
        return errorPage;
    }

    @Override
    public ErrorPage createErrorPageUnavailable() {
        ErrorPage errorPage =  super.createErrorPageUnavailable();
        createErrorPageContent(errorPage, ERROR_EXPIRED);
        return errorPage;
    }

    @Override
    public ErrorPage createErrorPageInternalServerError() {
        ErrorPage errorPage = super.createErrorPageInternalServerError();
        createErrorPageContent(errorPage, ERROR_500);
        return errorPage;
    }

    @Override
    public ErrorPage createErrorPageForbidden() {
        ErrorPage errorPage = super.createErrorPageForbidden();
        createErrorPageContent(errorPage, ERROR_FORBIDDEN);
        return errorPage;
    }

    /**
     * Override helper method to create example pages of DemoPageBlock class
     */
    @Override
    protected PageBlock createPage(String key, PageBlock parent, String vpathEN) {
        DefaultPageBlock page = new DefaultPageBlock();
        page.setKey(key);
        if (parent != null) {
            parent.getSubBlocks().add(page);
            page.setParentBlock(parent);
        }
        page.save();

        createNavItem(page, vpathEN, "en");
        return page;
    }

    private void createWidgetExampleBlock() {
        PageBlock homepage = (PageBlock) AbstractBlock.find.byKey(KEY_HOMEPAGE);
        try {
            CollectionBlock sidebar = (CollectionBlock) Template.addBlockToSlot(CollectionBlock.class, homepage, "sidebar");
            ContentBlock contentBlock = (ContentBlock)sidebar.addSubBlock(ContentBlock.class);
            contentBlock.getTitle().set("en", "Widgets");
            contentBlock.getContent().set("en",
                    "<p>Content filters are a great tool to let the user add variables freely inside any content, " +
                    "e.g. [[currentUserCount]] which your filter resolves to the current value " +
                    "when displaying the page.</p>\n" +
                    "<h4>Content filter example.</h4>\n" +
                    "<p>Last registered users: [[registeredUsersWidget:5]]</p>\n" +
                    "<p><a href=\"https://confluence.insign.ch/display/PLAY/Play+CMS\">" +
                    "Learn more </a> about filter framework</p>");
            contentBlock.save();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void createTeaserBlocks() {
        try {
            PageBlock homepage = (PageBlock) AbstractBlock.find.byKey(KEY_HOMEPAGE);
            CollectionBlock bottomPane = (CollectionBlock) Template.addBlockToSlot(CollectionBlock.class, homepage, "bottom");
            TeaserBlock block, block2, block3;
            block = (TeaserBlock)bottomPane.addSubBlock(TeaserBlock.class);
            block.getTitle().set("en", "Teaser block");
            block.getSubtitle().set("en", "Lorep Ipsum");
            block.getContent().set("en", "This is an example of customizing cms content blocks");
            block.getLogoUrl().set("en", "images/yacht.png");
            block.getLinkUrl().set("en", "confluence.insign.ch/display/PLAY/Create+your+own+cms+block");
            block.getLinkText().set("en", "Learn more");

            block2 = (TeaserBlock)bottomPane.addSubBlock(TeaserBlock.class);
            block2.getTitle().set("en", "Teaser block");
            block2.getSubtitle().set("en", "Lorep Ipsum");
            block2.getContent().set("en", "This is an example of customizing cms content blocks");
            block2.getLogoUrl().set("en", "images/yacht2.jpg");
            block2.getLinkUrl().set("en", "confluence.insign.ch/display/PLAY/Create+your+own+cms+block");
            block2.getLinkText().set("en", "Learn more");

            block3 = (TeaserBlock) bottomPane.addSubBlock(TeaserBlock.class);
            block3.getTitle().set("en", "Teaser block");
            block3.getSubtitle().set("en", "Lorep Ipsum");
            block3.getContent().set("en", "This is an example of customizing cms content blocks");
            block3.getLogoUrl().set("en", "images/yacht3.jpg");
            block3.getLinkUrl().set("en", "confluence.insign.ch/display/PLAY/Create+your+own+cms+block");
            block3.getLinkText().set("en", "Learn more");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void createCarInventoryBlock() {
        String frontendBlockKeyForCRUD = "_frontend_crud_example";
        GroupingBlock frontendGroupingBlock = GroupingBlock.find.frontend();

        CarInventoryBlock page = new CarInventoryBlock();
        page.setDealerTitle("Your favourite car dealer");
        page.setKey(frontendBlockKeyForCRUD);
        page.getPageTitle().set("en", "Car Inventory");
        page.getPageTitle().set("de", "Car Inventory");
        page.getMetaTitle().set("en", "Car Inventory");
        page.getMetaTitle().set("de", "Car Inventory");
        page.getNavTitle().set("en", "Car Inventory");
        page.getNavTitle().set("de", "Car Inventory");
        createNavItem(page, "/carInventory", "en");
        if (frontendGroupingBlock != null) {
            frontendGroupingBlock.getSubBlocks().add(page);
            page.setParentBlock(frontendGroupingBlock);
        }
        page.save();
    }

    private void createCrudExampleData() {
        Brand brand1 = new Brand();
        brand1.setTitle("Porsche");
        brand1.save();

        Brand brand2 = new Brand();
        brand2.setTitle("OPEL");
        brand2.save();

        Car car1 = new Car();
        car1.setModel("Carrera");
        car1.setBuyDate(Date.from(Instant.now()));
        car1.setBrand(brand1);
        car1.setPrice(15000);
        car1.setRegistrationId("AA 5676 CH");
        car1.save();

        Car car2 = new Car();
        car2.setModel("Boxter");
        car2.setBuyDate(Date.from(Instant.now()));
        car2.setBrand(brand1);
        car2.setPrice(2000);
        car2.setRegistrationId("M78699");
        car2.save();

        Car car3 = new Car();
        car3.setModel("Targa");
        car3.setBrand(brand1);
        car3.setPrice(11455);
        car3.setRegistrationId("CD ZH8 38");
        car3.save();

        Car car4 = new Car();
        car4.setModel("Cadet");
        car4.setBrand(brand2);
        car4.setPrice(10500);
        car4.setRegistrationId("CD Z56461");
        car4.save();
    }

    private void createErrorPageContent (ErrorPage errorPage, String errorMsg) {
        CollectionBlock pane = (CollectionBlock) Template.addBlockToSlot(
                CollectionBlock.class,
                errorPage,
                DefaultErrorPage.COLLECTION_SLOT
        );

        ContentBlock errorBlock = null;
        try {
            errorBlock = (ContentBlock)pane.addSubBlock(ContentBlock.class);
            pane.save();
            // Set the Error Text in Default Language
            MString contentString = new MString();
            contentString.set("en", errorMsg);

            errorBlock.setContent(contentString);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        errorBlock.save();
        pane.save();
        errorPage.save();
    }

    /**
     * Create an EmailTemplate for sending email to user after password reset
     */
    private void createErrorTemplates() {
        EmailTemplate template = new EmailTemplate();
        template.setTemplateKey("password.recovery.success");
        template.setSender("admin@localhost");
        template.setCategory(EmailTemplate.EmailTemplateCategory.EXTERN);
        template.getContent().set("en", "Hello {name}. A new password for your account has been created. ");
        template.getContent().set("de", "Hello {name}. A new password for your account has been created. ");
        template.getDescription().set("en", "New password has been created.");
        template.getDescription().set("en", "New password has been created.");
        template.getSubject().set("en", "New password has been created.");
        template.getSubject().set("de", "New password has been created.");
//        Sites.Site currentSiteInstance = CMS.getSites().current();
//        template.setSite(currentSiteInstance.key);

        template.save();
    }
}
