package blocks.mypageblock;

import blocks.myblock.MyContentBlock;
import ch.insign.cms.models.BlockFinder;
import ch.insign.cms.models.PageBlock;
import crud.models.Car;
import play.data.Form;
import play.mvc.Controller;
import play.twirl.api.Html;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "my_page_block")
@DiscriminatorValue("MyPageBlock")
public class MyPageBlock extends PageBlock {
    public static BlockFinder<MyPageBlock> find = new BlockFinder<>(MyPageBlock.class);
    public static final String CTX_LEFTCOL = "leftcol";
    public static final String CTX_RIGHTCOL = "rightcol";

    @Override
    public Html render() {
        return blocks.mypageblock.html.myPageBlock.render(this);
    }

    public Html editForm(Form editForm) {
        return blocks.mypageblock.html.myPageBlockEdit.render(this, editForm, Controller.request().getQueryString("backURL"), null);
    }
}
