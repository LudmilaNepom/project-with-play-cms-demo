package blocks.myblock;


import ch.insign.cms.models.BlockFinder;
import ch.insign.cms.models.ContentBlock;
import ch.insign.commons.db.MString;
import crud.models.Car;
import play.data.Form;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.twirl.api.Html;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "my_content_block")
@DiscriminatorValue("MyContentBlock")
public class MyContentBlock extends ContentBlock{
    public static BlockFinder<MyContentBlock> find = new BlockFinder<>(MyContentBlock.class);
    @OneToOne(cascade= CascadeType.ALL)
    private MString cars;

    public MString getCars() {
        return cars;
    }

    public void setCars(MString cars) {
        this.cars = cars;
    }

    @Override
    @Transactional
    public Html render() {
        return blocks.myblock.html.myContentBlock.render(this, Car.find.all());
    }

    @Override
    public Html editForm(Form editForm) {
        String backURL = Controller.request().getQueryString("backURL");
        return blocks.myblock.html.myContentBlockEdit.render(this, editForm, backURL, null);
    }


}
