$(document).ready(function() {
    function bindPathParamFromSelect() {
        $('#selectRowValue, #groupBrand').change(function (e) {
            e.preventDefault();
            window.location = $(this).children('option:selected').data('url');
        });
    }

    bindPathParamFromSelect();
});
