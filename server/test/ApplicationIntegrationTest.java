import org.junit.Test;
import play.Application;
import play.Mode;
import play.inject.guice.GuiceApplicationBuilder;
import play.libs.ws.WS;
import play.libs.ws.WSClient;
import play.libs.ws.WSResponse;
import play.test.Helpers;
import play.test.WithServer;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static play.mvc.Http.Status.*;
import static play.test.Helpers.*;

public class ApplicationIntegrationTest extends WithServer {

    @Override
    @SuppressWarnings("unchecked")
    public Application provideApplication() {
        return new GuiceApplicationBuilder()
                .in(Helpers.class.getClassLoader())// Take a class loader provided by Play! helper
                .in(Mode.TEST)// Set an environment mode for configuration
                .configure((Map) inMemoryDatabase())// Explicitly convert to Map, not Configuration
                .build();
    }

    /**
     * If the data hadn't reset, the server would throw internal(500) response status
     * Default port is 19001
     */
    @Test
    public void testShouldReturnServerErrorWithoutReset() {
        String url = "http://localhost:" + this.testServer.port() + "/";//FIXME change the url to our prod. base url
        try (WSClient ws = WS.newClient(this.testServer.port())) {
            CompletionStage<WSResponse> stage = ws.url(url).get();
            WSResponse response = stage.toCompletableFuture().get();
            assertEquals(INTERNAL_SERVER_ERROR, response.getStatus());
        } catch (IOException | InterruptedException | ExecutionException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testShouldReturnOk() throws Exception {
        String base = "http://localhost:" + this.testServer.port();
        try (WSClient ws = WS.newClient(this.testServer.port())) {
            String resetUrl = base + "/reset";
            String indexUrl = base + "/";

            CompletionStage<WSResponse> resetStage = ws.url(resetUrl).get();
            resetStage.thenAccept(responseConsumer -> assertTrue(responseConsumer.getStatus() == OK)).thenRun(() -> {
                CompletionStage<WSResponse> indexStage = ws.url(indexUrl).get();
                WSResponse response = null;
                try {
                    response = indexStage.toCompletableFuture().get();
                    assertEquals(OK, response.getStatus());
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            });
        }
    }

}
