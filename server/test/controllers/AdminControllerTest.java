package controllers;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mockito;
import play.Application;
import play.Mode;
import play.api.mvc.RequestHeader;
import play.inject.guice.GuiceApplicationBuilder;
import play.mvc.Http;
import play.mvc.Result;
import play.test.Helpers;
import play.test.WithApplication;
import util.TestDatabase;

import java.util.Collections;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static play.mvc.Http.Status.OK;
import static play.test.Helpers.*;

/**
 * According to the link, controller test is a part of unit tests
 * {@link https://www.playframework.com/documentation/2.5.x/JavaTest}
 */
public class AdminControllerTest extends WithApplication {

    @Rule
    public TestDatabase db = new TestDatabase();

    @Override
    @SuppressWarnings("unchecked")
    protected Application provideApplication() {
        return new GuiceApplicationBuilder()// Create a guice application with modules
                .in(Helpers.class.getClassLoader())// Take a class loader provided by Play! helper
                .in(Mode.TEST)// Set an environment mode for configuration
                .configure((Map) inMemoryDatabase())// Explicitly convert to Map, not Configuration
                .build();
    }

    @Override
    public void startPlay() {
        super.startPlay();
        Http.Context.current.set(new Http.Context(1L,
                Mockito.mock(RequestHeader.class),
                Mockito.mock(Http.Request.class),
                Collections.emptyMap(),
                Collections.emptyMap(),
                Collections.emptyMap()));
    }

    @Test
    public void testShouldInsertMockDataToDatabaseAndCheckUnauthorizedAccess() {
        Result result = route(fakeRequest(ch.insign.cms.controllers.routes.SetupController.reset()).header("Host", "localhost:9000"));
        assertEquals(OK, result.status());
        assertTrue(contentAsString(result).contains("Data initialized, now go to /"));// Unwrap content of html body and check on containing some data

        result = route(fakeRequest(controllers.routes.AdminController.myApp()).header("Host", "localhost:9000"));
        assertEquals(UNAUTHORIZED, result.status());// Due to fact that the user not signed(expected 401 handling)
        assertEquals("text/html", result.contentType().get());
        assertEquals("utf-8", result.charset().get());
    }

}
