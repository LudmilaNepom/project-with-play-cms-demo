import sbt._

/**
  * Adds the ability to have conditional dependencies to other projects.
  * Dependig on the {@code local} boolean parameter, the dependencies will be
  * loaded either from the remote ivy repository or directly from the local sub-project.
  *
  * TODO: this code duplicates play-cms/project/Modules.scala which should be fixed.
  *
  * @param local if true, ProjectRef will be loaded, otherwise ModuleID will be used
  * @param deps the tuples with ModuleID and related ProjectRef
  */
class Modules(local: Boolean, deps :(ModuleID, ProjectRef)*)
{
  def apply (p :Project) = (deps collect {
    case (_, ref) if local => ref
  }).foldLeft(p) { (l, r) =>  l.dependsOn(r).aggregate(r) }

  def libraryDependencies = deps collect {
    case (lib, _) if !local => lib
  }
}

object Modules
{
  def apply (local: Boolean)(deps :(ModuleID, ProjectRef)*) = new Modules(local, deps :_*)
}
