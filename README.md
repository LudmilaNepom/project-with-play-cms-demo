# Play-CMS Demo #

This is a Play framework demo application built with [insign](http://www.insign.ch)'s **play-cms** - a java-based Play framework CMS.

This demo creates some demo content and uses the h2 in-memory db for storage. It is intended to be used as quick-and-easy starting point for your own play-cms project. We've also added a docker-based devops setup for a CI (continuous integration) process.

Check out the [play-cms](http://www.play-cms.com) website for more information and documentation or visit the [play-cms community](https://plus.google.com/u/0/communities/100224277865065215900/)


## Quick start

Either do a local installation (if you have the play framework installed) using H2 by default, or run it in Docker which uses MySQL database by default.

### Local installation ###

Make sure you have the play framework 2.4+ installed. Checkout the project and run

    sbt run

By default application will be running on http://127.0.0.1:9000/ and uses a H2 in-memory db for (non-persistent) storage.

### Running in docker container ###
If you want to run play-cms-demo in a docker container, create a docker image with

    sbt docker:publishLocal

command, or pull the ready image from bintray

    docker pull insign-docker-registry.bintray.io/play-cms-demo:1.1

After that run the application with

    docker run -p 9000:9000 insign-docker-registry.bintray.io/play-cms-demo:1.1

By default, the container will be running on http://127.0.0.1:9000/ 

The MySQL db can be viewed via PhpMyAdmin spinning on http://127.0.0.1:8080/

### Backend Access ###
Default admin page url: http://127.0.0.1:9000/admin

### Admin's credentials ###
login: admin@insign.ch

password: 123456

## Development environment and Continuous Integration ##
The project contains both a development environment and continuous integration setup (based on docker) for quick-starting new projects. 

### Developing the play modules ##
 If you want to develop the play-* modules, you need to make next steps:

* Clone play-cms into a project root:
```
#!bash

git clone git@bitbucket.org:insigngmbh/play-cms.git
```

* Create a file ".sbtopts" in your project root with the following content **-J-Dplaycms.devmode=true**

### Use the development environment ###

Make sure you have docker and docker-compose installed.


Run 
```
devops/app.sh
```

This will build, launch and link the required containers and mount your code folder using docker-compose:

* App container, with mounted project code folder, exposed on Port 9000
* MySQL container and play db
* PhpMyAdmin container at localhost:8080 (user: playcmsdemo, pw: admin)

The App container is launched in daemon mode, if you want to launch it normally, pass the option *up* (default is *up -d*):
```
devops/app.sh up
``` 
This will give you the console-output of the running app and you can quit the app with CTRL+C

Daemonized Docker container can be shut down with

```
devops/app.sh down
``` 

(For more details, check out devops/docker-compose.yml and common.yml)

If you want to add extra SBT_OPTS parameters to the SBT-Session inside docker, you can do this as follows

```
SBT_OPTS="-Dextra.option=val" devops/app.sh up
```
This will start SBT with extra-options.


To connect to your app container:

```
docker exec -it playcmsdemo_app_1 bash
```

The project code folder is mounted. Use the usual sbt commands to work with your code.


### CI setup with Jenkins ###
Jenkins container with already configured job which builds and tests demo application on every commit can be run with: 
```
cd devops; ./jenkins.sh
```

Its web-interface is exposed on port 8083


## Developing the demo project itself ##

### Publishing modules to bintray ###
Modules can be published to bintray with

    sbt publish

Credentials will be asked on first time. 

Note: bintray doesn't support snapshots.

Current version of a module can be easily removed from bintray with

    sbt bintrayUnpublish

### Publishing docker image to bintray registry ###
Next command will upload a docker image of play-cms-demo to insign's bintray registry:

    sbt docker:publish