package frontend

import frontend.services.AjaxClient
import org.querki.jquery.{$, _}
import org.scalajs.dom._
import playcms.shared.Api

import scala.scalajs.js
import scala.scalajs.js._
import scala.scalajs.js.timers._
import scala.scalajs.js.annotation._
import js.Dynamic.{global => g}
import js.Dynamic.{literal => l}
import scala.scalajs.runtime.UndefinedBehaviorError
import autowire._
import boopickle.Default._
import com.karasiq.bootstrap.form.FormInputGroup
import frontend.controllers.UserController
import logger._

import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue
import org.scalajs.dom
import dom.document

import scalatags.JsDom._
import scalatags.JsDom.all._
import com.karasiq.bootstrap.modal.Modal
import com.karasiq.bootstrap.{Bootstrap, BootstrapHtmlComponent}
import rx._


@JSExport("App")
object App extends js.JSApp {
  implicit def jq2AjaxForm(jq: JQuery): AjaxForm = jq.asInstanceOf[AjaxForm]

  /**
    * Main Entry-Point when not triggered by Script-Tag in Html-Head
    */
  @JSExport
  def main() = {
    timers.setInterval(100)(() => bootstrap())
    log.enableServerLogging("/logging")
    log.warn("User started main Method")
    $(() => modalOnClick())
    $("document").ready(() => bootstrap())
  }

  /**
    * starts the bootstrapping process by calling init on all frontend-related classes
    */
  @JSExport("bootstrap")
  def bootstrap(): Unit = {
    console.log("starting frontend application")
    console.log("starting frontend application")

    UserController.init

    AjaxClient[Api].simpleAjaxCall("return me this string").call().map((response) => console.log("Server responded with: " + response))

    UserController.logUsers()
  }

  def modalOnClick(): Unit = {
    $(".col-sm-3 button").on("click", ((btn: dom.Element) => showModal2(btn: dom.Element)))
  }

  def showModal2(btn: dom.Element): Unit = {
    $(btn).next().prop("style", "display: none;")
    val blockid = $(btn).attr("id")
    val body: Modifier = form(id := "carstextform",
      div(`class` := "form-group",
        label(`class` := "control-label", "cars", `for` := "cars"),
        input(`class` := "form-control", id := "cars", placeholder := "input some text")
      )
    )
    Modal()
      .withTitle("Test title")
      .withBody(body)
      .withButtons(Modal.closeButton(), Modal.button("Submit", Modal.dismiss, onclick := Bootstrap.jsClick { _ =>



        $("#carstextform").ajaxForm(AjaxSubmitOptions
          .url("/admin/editMyContentBlock/"+blockid)
          .`type`("POST")
          .data(js.Dictionary("cars" -> $("#carstextform").find("input[id='cars']").`val`()))
          .success({ (responseText: js.Any, statusText: String, xhr: JQueryXHR, form: org.querki.jquery.JQuery) => {
              statusText match {
              case "success" => {
                $(btn).prev().remove()
                $(btn).prev().after("<h4>" + responseText.asInstanceOf[String] + "</h4>")
              }
            }
          }
          })
          .error({ (xhr: JQueryXHR) => {
            val response = xhr.statusText.asInstanceOf[String]
            println(response)
            response match {
            case "Forbidden" => {
                $(btn).next().prop("style", "display: run-in;")
              }
            }
          }
          })
        )

          $ ("#carstextform").submit()

      }
      ))
      .show(backdrop = false)
  }

}
