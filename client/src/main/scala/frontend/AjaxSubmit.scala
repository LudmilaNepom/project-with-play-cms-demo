package frontend

import org.querki.jquery.JQuery
import org.querki.jsext.{JSOptionBuilder, OptMap, noOpts}
import org.scalajs.dom
import org.scalajs.dom.XMLHttpRequest
import scala.scalajs.js
/**
  * Facade for AjaxForm;
  */
@js.native
trait AjaxForm extends org.querki.jquery.JQuery {
  def ajaxSubmit(options: AjaxSubmitOptions = null): AjaxForm = js.native

  def ajaxForm(options: AjaxSubmitOptions = null): AjaxForm = js.native
}

@js.native
trait AjaxSubmitOptions extends js.Object

object AjaxSubmitOptions extends AjaxSubmitOptionsBuilder(noOpts)

class AjaxSubmitOptionsBuilder(val dict: OptMap) extends JSOptionBuilder[AjaxSubmitOptions, AjaxSubmitOptionsBuilder](new AjaxSubmitOptionsBuilder(_)) {

  def error(v: js.Function1[org.querki.jquery.JQueryXHR, Any]) = jsOpt("error", v)

  def clearForm(v: Boolean) = jsOpt("clearForm", v)

  def resetForm(v: Boolean) = jsOpt("resetForm", v)

  def beforeSubmit(v: js.Function3[js.Any, org.querki.jquery.JQuery, js.Any, Boolean]) = jsOpt("beforeSubmit", v)

  def beforeSerialize(v: js.Function2[org.querki.jquery.JQuery, js.Any, Boolean]) = jsOpt("beforeSerialaze", v)

  def success(v: js.Function4[js.Any, String, org.querki.jquery.JQueryXHR, org.querki.jquery.JQuery, Any]) = jsOpt("success", v)

  def data(v:js.Dictionary[js.Any]) = jsOpt("data", v)

  def url(v:String) = jsOpt("url", v)

  def target(v:org.querki.jquery.JQuery) = jsOpt("target", v)

  def `type`(v:String) = jsOpt("type", v)

  def uploadProgress(v:js.Function4[dom.Event, Int, Int, Int, Any]) = jsOpt("uploadProgress", v)

  def semantic(v: Boolean) = jsOpt("semantic", v)

  def forceSync(v: Boolean) = jsOpt("forceSync", v)

  def iframe(v: Boolean) = jsOpt("iframe", v)

  def iframeSrc(v:String) = jsOpt("iframeSrc", v)

}






