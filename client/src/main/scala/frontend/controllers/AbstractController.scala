package frontend.controllers

import autowire._
import boopickle.Default._

import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue

/**
  * @author Urs Honegger &lt;u.honegger@insign.ch&gt;
  */
trait AbstractController {
  var serversideLogging = false
  def enableServersideLogging(boolean: Boolean) = serversideLogging = boolean
}
